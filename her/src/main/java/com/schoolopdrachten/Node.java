/**Francesca Wiegman**/
/*Studentnummer 602501*/
/*Opdracht PO_Algoritmes*/
/*HBO-ICT Deeltijd - ASD*/
package com.schoolopdrachten;

// Bron: https://www.baeldung.com/java-binary-tree

// Aanmaken van een class met de Nodes (bladeren) Deze slaat de nodes op als integers met een referentie naar elke
// child node (kinderen)
public class Node {
    int value;
    Node left;
    Node right;


    Node(int value){
        this.value = value;
        right = null;
        left = null;
    }

}
