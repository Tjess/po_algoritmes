import com.schoolopdrachten.GenericQuickSort;
import org.junit.Assert;
import org.junit.Test;

public class GenericQuickSortTest {

        @Test
        public void when_quickSort_OneElement_ArrayIsReturned() {
            // Arrange
            Integer[] array = new Integer[]{2};
            Integer[] expectedArray = new Integer[]{2};

            // Act
            GenericQuickSort.quickSort(array, 2, 2);

            // Assert
            Assert.assertArrayEquals(expectedArray, array);
            //Assert.assertEquals(new Integer(2).intValue(), array[0].intValue());
        }

        @Test
        public void quickSort_with_empty_array() {
            // Arrange
            Integer[] array = new Integer[]{};
            Integer[] expectedArray = new Integer[]{};

            // Act
            GenericQuickSort.quickSort(array, 0, 0);

            // Assert
            Assert.assertArrayEquals(expectedArray, array);
        }
        @Test
        public void quickSort_with_no_values_in_array(){
            // Arrange
            Integer [] array = new Integer[0] ;
            Integer [] expectedArray = new Integer[0];

            // Act
            GenericQuickSort.quickSort(array, 0, 0);

            // Assert
            Assert.assertArrayEquals(expectedArray, array);
        }
        @Test
         public void quickSort_with_10_values_need_to_be_sorted() {
            //Arrange
            Integer [] array = new Integer[] {25,10,5,30, 21, 12, 28, 33, 40, 8};
            Integer [] expectedArray = new Integer[] {5,8,10,12,21,25,28,30,33,40};

            // Act
            GenericQuickSort.quickSort(array, 0,array.length-1);


            // Assert
            Assert.assertArrayEquals(expectedArray, array);
        }

        @Test
        public void quicksort_with_2_values() {
                // Arrange
                Integer [] array = new Integer[] {15,10};
                Integer[] expectedArray = new Integer[] {10,15};

                //Act
                GenericQuickSort.quickSort(array, 0, array.length-1);

                //Assert
                Assert.assertArrayEquals(array,expectedArray);
        }
// Test 1: Lege array (NULL)
            //test2 : array ZONDER waardes (dus LEEG)
            //test 3: met 1 element // DONE
            //Test 4: met 2 elementen
            //Test 5: Met 10 elemetne, resultaat moet de gewenste volgorde zijn.
            // assert statements
           // assertEquals(0, 10, "10 x 0 must be 0");
            //assertEquals(0, tester.multiply(0, 10), "0 x 10 must be 0");
            //assertEquals(0, tester.multiply(0, 0), "0 x 0 must be 0");

}
