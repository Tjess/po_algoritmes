/**Francesca Wiegman**/
/*Studentnummer 602501*/
/*Opdracht PO_Algoritmes*/
/*HBO-ICT Deeltijd - ASD*/
package com.schoolopdrachten;


// Geeft de edges van een Graph weer
 // Basis item in een adjacent list
 // Bron: Data_Structures_and_Problem_Solving_Using_Java__4ed__Weiss (pdf)
public class Edge {
    public Vertex dest;     // Tweede Vertex in een edge
    public double cost;     // De weging van een edge

    public Edge (Vertex d,double c) {
        dest = d;
        cost = c;
    }
}
