/**Francesca Wiegman**/
/*Studentnummer 602501*/
/*Opdracht PO_Algoritmes*/
/*HBO-ICT Deeltijd - ASD*/
package com.schoolopdrachten;

// Dit is deels overgenomen uit het boek van Weis.
// Data_Structures_and_Problem_Solving_Using_Java__4ed__Weiss
// Graph class: Evaluaren kortste pad
//
// CONSTRUCTION: with no parameters.
//
// ******************PUBLIC OPERATIONS**********************
// void addEdge( String v, String w, double cvw )
// --> Add additional edge
// void printPath( String w ) --> Print path after alg is run
// void unweighted( String s ) --> Single-source unweighted
// void dijkstra( String s ) --> Single-source weighted

import java.util.*;

public class Graph {
    // Deze is nodig wanneer een vertex niet bereikbaar is.
    public static final double INFINITY = Double.MAX_VALUE;

    // Het toevoegen van nieuwe edges
    public void addEdge(String sourceName, String destName, double cost) {
        Vertex v = getVertex(sourceName);
        Vertex w = getVertex(destName);
        v.adj.add(new Edge(w, cost));
        System.out.println("Sourcename: "+sourceName + " ==>  Destination: " + destName);
    }

    // recursieve aanroep het handelen van onbereikbare Vertexen en het printen van de totale weging.
    // Wordt aangeroepen nadat het kortste pad algoritme zijn werk heeft gedaan.
    public void printPath(String destName) {
        System.out.println("destName in printpath:" + destName);

        Vertex w = vertexMap.get(destName);

        // Wanneer w geen waardes heeft, dan wordt er een error gegooid.
        // Dit is een standaard error van Java.
        if (w == null) {
            throw new NoSuchElementException();
        }
        // Wanneer de afstand van een Vertex oneindig is, dan is deze onbereikbaar. Dit is dan een
        // isolated Vertex
        else if (w.dist == INFINITY) {
            System.out.println(destName + " is onbereikbaar");
        }
        // Wanneer de Vertex bestaat en de afstand niet oneindig is, wordt het gehele pad afgedrukt in de console.
        else {
            System.out.print("(Cost is: " + w.dist + ") ");
            printPath(w);

            System.out.println();

        }
    }

    // Methode om de naam van de Vertex te krijgen
    public String getVertexName(){
    String name;
    name = vertexMap.toString();
     System.out.println("naam: " + name);

    return name;
    }

    // Als de Vertex niet bestaat, dan returnt het null (dat is niet erg)
    public Vertex getVertexByName(String name){
        return vertexMap.get(name);
    }

    // Geeft het aantal Vertices uit de VertexMap terug
    public int getGraphSize() {
        return vertexMap.size();
    }

    // Het algoritme voor een unweighted shortest path.
    public void unweighted(String startName) {
        // Haal eerst alles leeg
        // clearAll( );
        // Startnaam wordt opgegeven door de gebruiker in de main. Wanneer er niets opgegeven wordt,
        // dan wordt er een exceptie fout gegeven
        Vertex start = vertexMap.get(startName);
        if (start == null)
            throw new NoSuchElementException("Start vertex niet gevonden");
        //
        Queue<Vertex> q = new LinkedList<Vertex>();
        q.add(start);
        start.dist = 0;

        while (!q.isEmpty()) {
            Vertex v = q.remove();

            for (Edge e : v.adj) {
                Vertex w = e.dest;

                if (w.dist == INFINITY) {
                    w.dist = v.dist + 1;
                    w.prev = v;
                    q.add(w);
                }
            }
        }
    }
  // Het Dijkstra algoritme
    public void dijkstra(String startName) {
        PriorityQueue<Path> pq = new PriorityQueue<Path>();

        Vertex start = vertexMap.get(startName);
        if (start == null)
            throw new NoSuchElementException("Start vertex niet gevonden");

        clearAll();
        pq.add(new Path(start, 0));
        start.dist = 0;

        int nodesSeen = 0;
        while (!pq.isEmpty() && nodesSeen < vertexMap.size()) {
            Path vrec = pq.remove();
            Vertex v = vrec.dest;
            if (v.scratch != 0) // already processed v
                continue;

            v.scratch = 1;
            nodesSeen++;

            for (Edge e : v.adj) {
                Vertex w = e.dest;
                double cvw = e.cost;

                if (cvw < 0)
                    throw new GraphException("Graph heeft negatieve edges");

                if (w.dist > v.dist + cvw) {
                    w.dist = v.dist + cvw;
                    w.prev = v;
                    pq.add(new Path(w, w.dist));
                }
            }
        }
    }

    public void clearGraph()
    {
        vertexMap.clear();
    }

    // check of een Vertex bestaat, zo ja, geef de vertex terug, zo nee, maak een nieuwe vertex aan
    // en geef deze terug
    private Vertex getVertex(String vertexName) {
        Vertex v = vertexMap.get(vertexName);
        if (v == null) {
            v = new Vertex(vertexName);
            vertexMap.put(vertexName, v);
        }
        return v;
    }


    // zorgt ervoor dat het path in de console geprint wordt. (recursief)
    // als vorige plek niet null is, dan print deze het pad van de vorige Vertex
    // wanneer er geen vorige Vertex is, print deze de naam van de dest.

    private void printPath(Vertex dest) {
        if (dest.prev != null) {
            printPath(dest.prev);
            System.out.print(" to ");
        }
        System.out.print(dest.name);
        }

    // Zorgt dat alle data "opgeruimd" wordt voor het algoritme voor het kortste pad aangeroepen wordt.
    private void clearAll() {
        for (Vertex v : vertexMap.values())
            v.reset();
    }

    private Map<String, Vertex> vertexMap = new HashMap<String, Vertex>();
}


// Used to signal violations of preconditions for
// various shortest path algorithms.
class GraphException extends RuntimeException {
    public GraphException(String name) {
        super(name);
    }
}

