/**Francesca Wiegman**/
/*Studentnummer 602501*/
/*Opdracht PO_Algoritmes*/
/*HBO-ICT Deeltijd - ASD*/
package com.schoolopdrachten;

public final class GenericQuickSort {


   // Statische generieke methode om een lijst getallen te sorteren van klein naar groot.
    // De comparable maakt het mogelijk om getallen met elkaar te vergelijken. Er is een
    // begin getal en een eind getal. De pivot wordt gekozen in het midden.
    // Wanneer het begin kleiner is het einde, wordt een pivot gekozen.
    // het begin wordt vergeleken met de pivot en wanneer kleiner dan 0, gaat het begin een stap verder.
    // het eindgetal wordt vergeleken met de pivot en of deze kleiner dan nul is, zo ja, dan gaat het eind
    // een stapje terug.
    // De waardes van begin en eind worden met de pivot vergeleken. Wanneer begin groter is dan de pivot, wordt
    // deze rechts van de pivot gezet, wanneer kleiner, dan links. Hetzelfde geldt voor het eind. Is deze groter
    // dan de pivot, blijft ie staan, is die kleiner, wordt het links van de pivot gezet. Er wordt een nieuwe pivot gekozen
    // en alles herhaalt zich tot de inhoud van de array is gesorteerd.

    public static <T extends Comparable<T>> void quickSort(T[] arr, int begin, int eind) {
        if (begin < eind) {
            int i = begin, j = eind;
            T pivot = arr[(begin + eind) / 2];
            System.out.println("begin: " + begin + " eind: " + eind + " - pivot: " + pivot);
            do {
                while (arr[i].compareTo(pivot) < 0) i++;
                while (pivot.compareTo(arr[j]) < 0) j--;
                if (i <= j) {
                    T tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                    i++;
                    j--;
                }
            } while (i <= j);

            // Recursieve aanroep voor het sorteren van de inhoud van de array. Deze stopt wanneer
            // alle getallen op de juiste plek staan.
            quickSort(arr, begin, j);
            quickSort(arr, i, eind);
        }
    }
}
// Gebaseerd op : http://www.learntosolveit.com/java/GenericQuicksortComparable.html