/**Francesca Wiegman**/
/*Studentnummer 602501*/
/*Opdracht PO_Algoritmes*/
/*HBO-ICT Deeltijd - ASD*/
package com.schoolopdrachten;


import java.util.LinkedList;
import java.util.List;

// Bron: Data_Structures_and_Problem_Solving_Using_Java__4ed__Weiss (pdf)
public class Vertex {
    public String name;         //Naam van de Vertex
    public List<Edge> adj;      // Adjacent Edges
    public double dist;        // Weging van de edges
    public Vertex prev;         // Vorige vertex van het kortste pad
    public int scratch;         // Extra variable used in algorithm

    public Vertex(String p_naam) {
        name = p_naam;
        adj = new LinkedList<Edge>();
        reset();
    }

    public void reset() {
        dist = Graph.INFINITY;
        prev = null;
        //pos = null;
        scratch = 0;
    }
}
