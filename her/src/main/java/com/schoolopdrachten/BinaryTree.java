/**Francesca Wiegman**/
/*Studentnummer 602501*/
/*Opdracht PO_Algoritmes*/
/*HBO-ICT Deeltijd - ASD*/

package com.schoolopdrachten;

// Deze binary tree is gebasseerd op de tutorial op : https://www.baeldung.com/java-binary-tree

public class BinaryTree {

    // Creeëren van een startpunt. Dit wordt over het algemeen de root genoemd

    Node root;

    // Het recursief toevoegen van Nodes aan de tree.
    // Wanneer de waarde (value) lager is dan de huidige waarde, dan wordt de Node aan de linkerkant
    // toegevoegd. Wanneer de waarde hoger is, wordt deze aan de rechterkant van de boom toegevoegd.
    // Is de huidige waarde null, dan is er een lege plek bereikt en kan de Node toegevoegd worden.


    public void add(int value) {
        root = addRecursive(root, value);
    }

    private Node addRecursive(Node current, int value) {

        //als de huidige node leeg is, voeg waarde toe
        if (current == null) {
            return new Node(value);
        }
        // als de waarde lager is dan de huidge node, voeg node toe aan de linker kant
        if (value < current.value) {
            current.left = addRecursive(current.left, value);
            // als de waarde hoger is dan de huidge node, voeg node toe aan de rechter kant
        } else if (value > current.value) {
            current.right = addRecursive(current.right, value);
        }
        return current;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public int getSize() {
        return getSizeRecursive(root);
    }

    private int getSizeRecursive(Node current) {
        return current == null ? 0 : getSizeRecursive(current.left) + 1 + getSizeRecursive(current.right);
    }


    public boolean containsNode(int value) {
        return containsNodeRecursive(root, value);
    }

    private boolean containsNodeRecursive(Node current, int value) {
        if (current == null) {
            return false;
        }

        if (value == current.value) {
            return true;
        }

        return value < current.value
                ? containsNodeRecursive(current.left, value)
                : containsNodeRecursive(current.right, value);
    }

    public void delete(int value) {
        root = deleteRecursive(root, value);
    }

    private Node deleteRecursive(Node current, int value) {
        if (current == null) {
            return null;
        }

        if (value == current.value) {
            // Case 1: no children
            if (current.left == null && current.right == null) {
                return null;
            }

            // Case 2: only 1 child
            if (current.right == null) {
                return current.left;
            }

            if (current.left == null) {
                return current.right;
            }

            // Case 3: 2 children
            int smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursive(current.right, smallestValue);
            return current;
        }
        if (value < current.value) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }

        current.right = deleteRecursive(current.right, value);
        return current;
    }

    public int findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }
    public void printSmallestValue(){
        System.out.println("kleinste waarde: " + findSmallestValue(root));
    }
    public int findHighestValue(Node root) {
        return root.right == null ? root.value : findHighestValue(root.right);}
    public void printHighestValue() {
        System.out.println("hoogste waarde: " + findHighestValue(root)); }


    private void visit(int value) {
        System.out.print(" " + value);
    }

    class Node {
        int value;
        Node left;
        Node right;

        Node(int value) {
            this.value = value;
            right = null;
            left = null;
        }
    }
}

