package com.schoolopdrachten;

import org.junit.Assert;
import org.junit.Test;

import java.util.NoSuchElementException;
//import org.junit.jupiter.api.Test;
//import exceptions.GraphException;

public class GraphTest {

    private Graph graph = new Graph();

    @Test
    public void When_AddEdge_VerticesDoNotExist_VerticesAndEdgeAreAdded()
    {
        // Arrange
        graph.clearGraph();

        // Act
        graph.addEdge("v1", "v2", 3);

        // Assert
        Assert.assertEquals("v1", graph.getVertexByName("v1").name);
        Assert.assertEquals("v2", graph.getVertexByName("v2").name);
        Assert.assertEquals(1, graph.getVertexByName("v1").adj.size());
        Assert.assertEquals(2, graph.getGraphSize());
    }

    @Test
    public void When_AddEdge_VerticesDoExist_EdgeIsAdded()
    {
        // Arrange
        graph.clearGraph();
        graph.addEdge("v1", "v2", 3);

        // Act
        graph.addEdge("v2", "v1", 7);

        // Assert
        Assert.assertEquals(1, graph.getVertexByName("v2").adj.size());
        Assert.assertEquals(2, graph.getGraphSize());
    }

    @Test (expected = NoSuchElementException.class)
    public void When_Unweighted_AndStartingNodeNotFound_Exception(){
        // Arrange
        graph.clearGraph();

        // Act
        graph.unweighted("I don't exist");
    }


@Test
public void TestUnweighted_where_destination_not_is_connected(){
        //Arrange
    graph.clearGraph();
    buildTestGraph();
     String dest = "V0";

    //Act
    graph.unweighted("V5");
    //Assert
   Assert.assertEquals(graph.INFINITY, graph.getVertexByName(dest).dist, 0.1);
   Assert.assertNull(graph.getVertexByName(dest).prev);
}

    @Test
    public void When_Unweighted_AndPathExists_PathIsFound()
    {
        // Arrange
        graph.clearGraph();
        buildTestGraph();

        // Act
        graph.unweighted("V0");

        //Assert
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(1, graph.getVertexByName("V1").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V2").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(1, graph.getVertexByName("V3").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V4").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V5").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V6").dist, 0.1);

    }


    // Normale unweighted test die wel path geeft (dus correcte flow)

    // Dezelfde 3 tests, maar dan voor Dijksta

    // Nog een Dijkstra test, maar er is een exceptie omdat er een negatieve waarde voor een edge bestaat.

    @Test(expected = NoSuchElementException.class)
    public void when_startname_isnull_in_unweighted_throw_exception() {
        // Arrange
        graph.clearGraph();
        buildTestGraph();
        Vertex start = (new Vertex(" "));
        Vertex expectedStart = (new Vertex("v1"));

        // Act
        graph.unweighted(null);

        // Assert
        Assert.assertEquals(expectedStart, start);
    }
// Dijkstra Testen

    @Test (expected = NoSuchElementException.class)
    public void When_Dijkstra_AndStartingNodeNotFound_Exception(){
        // Arrange
        graph.clearGraph();

        // Act
        graph.dijkstra("I don't exist");
    }

    @Test
    public void TestDijkstra_where_destination_not_is_connected(){
        //Arrange
        graph.clearGraph();
        buildTestGraph();
        String dest = "V0";

        //Act
        graph.dijkstra("V5");
        //Assert
        Assert.assertEquals(graph.INFINITY, graph.getVertexByName(dest).dist, 0.1);
        Assert.assertNull(graph.getVertexByName(dest).prev);
    }

    @Test
    public void When_Dijkstra_AndPathExists_PathIsFound()
    {
        // Arrange
        graph.clearGraph();
        buildTestGraph();

        // Act
        graph.dijkstra("V0");

        //Assert
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(1, graph.getVertexByName("V1").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V2").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(1, graph.getVertexByName("V3").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V4").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V5").dist, 0.1);
        Assert.assertEquals(graph.getVertexByName("V0"), graph.getVertexByName("V1").prev);
        Assert.assertEquals(2, graph.getVertexByName("V6").dist, 0.1);

    }
    // Test Dijkstra met negatieve edge

    @Test (expected = GraphException.class)
            public void test_Dijkstra_negatieve_edge_exception () {
        // Arrange
        graph.clearGraph();
        graph.addEdge("V0", "V2", -10);
       // buildTestGraph();

        //Act
        graph.dijkstra("V0");
    }


    // Opbouwen van een graph voor het testen
    private void buildTestGraph(){
        graph = new Graph();
        graph.addEdge("V0", "V1", 1);
        graph.addEdge("V0", "V3", 1);
        graph.addEdge("V1", "V3", 1);
        graph.addEdge("V1", "V4", 1);
        graph.addEdge("V2", "V0", 1);
        graph.addEdge("V2", "V5", 1);
        graph.addEdge("V3", "V2", 1);
        graph.addEdge("V3", "V4", 1);
        graph.addEdge("V3", "V5", 1);
        graph.addEdge("V3", "V6", 1);
        graph.addEdge("V4", "V6", 1);
        graph.addEdge("V6", "V5", 1);

    }
}