package com.schoolopdrachten;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinaryTreesTest {
    private BinaryTree bt = new BinaryTree() ;
    @Test
    public void add_Node_to_Binary_Tree() {
        // arrange

        // act
        bt.add(72);
        bt.add(35);
        bt.add(2);

        // assert
        Assert.assertEquals(72, 72);
        Assert.assertEquals(35, 35);
        Assert.assertEquals(2, 2);
        Assert.assertEquals(3,bt.getSize());

    }

    @Test
    public void getSize_of_the_BinaryTree_after_building() {
        // arrange

        //act
        buildTestBinaryTree();

        // assert
        Assert.assertEquals(9, bt.getSize());
    }

    @Test
    public void check_if_BinaryTree_containsNode() {
        // arrange
        buildTestBinaryTree();

        //act
        bt.containsNode(96);

        // assert
        Assert.assertEquals(96,96);
    }

    @Test
    public void binaryTree_does_not_contains_node() {
        // arrange
        buildTestBinaryTree();

        // act
        bt.containsNode(3);

        // assert
        Assert.assertFalse(false);
    }

    @Test
   public void findSmallestValue_in_BinaryTree() {
        // arrange
        buildTestBinaryTree();

        //act
        bt.findSmallestValue(bt.root);

        // assert
        Assert.assertEquals(2, 2);
    }


    @Test
    public void findHighestValue_in_BinaryTree() {
        // arrange
        buildTestBinaryTree();

        // act
        bt.findHighestValue(bt.root);

        // assert
        Assert.assertEquals(96,96);
    }

    @Test
    public void delete_value_from_BinaryTree() {
        // arrange
        buildTestBinaryTree();

        //act
        bt.delete(35);
        bt.containsNode(35);

        // assert

        Assert.assertEquals(8, bt.getSize());
        Assert.assertFalse(false);

    }
    // Build Test Tree
    private void buildTestBinaryTree() {
        bt.add(11);
        bt.add(72);
        bt.add(35);
        bt.add(2);
        bt.add(96);
        bt.add(93);
        bt.add(28);
        bt.add(72);
        bt.add(24);
        bt.add(14);
    }
}