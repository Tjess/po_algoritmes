/**
 * Francesca Wiegman
 **/
/*Studentnummer 602501*/
/*Opdracht PO_Algoritmes*/
/*HBO-ICT Deeltijd - ASD*/

// Er staan een aantal System.out.println in commentaar in de bestanden.
// Dit heb ik gebruikt om voor mezelf te controleren of alles klopt.
// Ik heb deze laten staan, maar in commentaar gezet.

// Ik maak bij het aanroepen van de quicksort gebruik van een functie om een array te vullen
// met random getallen. Hierdoor is in feite elke keer dat het programma draait een andere lijst
// met getallen beschikbaar.

package com.schoolopdrachten;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;

public class Main {

// Methode om een array aan te maken met random, ongesorteerde getallen

    public static Integer[] getArray() {
        int size = 10;
        Integer[] array = new Integer[size];
        int item = 0;
        for (int i = 0; i < size; i++) {
            item = (int) (Math.random() * 100);
            array[i] = item;
        }
        return array;
    }

    public static void main(String[] args) {
        Integer[] getallen = getArray();
        int counter = 0;
        //Ongesorteerde array
        System.out.println("Ongesorteerde Array");
        // zorgen dat de getallen op één regel staan in plaats van onder elkaar
        for (Integer i : getallen) {
            System.out.print(i);
            if (counter < getallen.length - 1) {
                System.out.print(",");
            } else {
                System.out.println();
            }
            counter++;

        }
        GenericQuickSort.<Integer>quickSort(getallen, 0, getallen.length - 1);
        //gesorteerde array
        counter = 0;
        System.out.println("Gesorteerde Array");
        // zorgen dat de getallen op één regel staan in plaats van onder elkaar
        for (Integer i : getallen) {
            System.out.print(i);
            if (counter < getallen.length - 1) {
                System.out.print(",");
            } else {
                System.out.println();
            }
            counter++;
        }
        // Aanmaken van een Binary Tree. Dit had misschien beter in de class van BinaryTree gekunt.
        // Ik gebruik normaal geen JAVA, dus moest dit nog even goed uitvinden. Heb niet meer de tijd gehad
        // om het aan te passen.
        BinaryTree bt = new BinaryTree();

        bt.add(11);
        System.out.println("rootValue: " + bt.root.value);
        bt.add(72);
        bt.add(35);
        bt.add(2);
        bt.add(96);
        bt.add(93);
        bt.add(28);
        bt.add(72);
        bt.add(24);
        bt.add(14);

        bt.getSize();
        //        System.out.println("size van bt should be 9: "+ bt.getSize());
        //       System.out.println( "bevat Node 96: " + bt.containsNode(96));
        if (bt.root.left != null || bt.root.right != null)
            //      { System.out.println("left: " + bt.root.left.value + " -> right: " + bt.root.right.value);     }
            bt.printSmallestValue();
        bt.printHighestValue();
        bt.delete(96);
        //      System.out.println( "bevat Node 96: " + bt.containsNode(96));
        //     System.out.println("size van bt should be 8: "+ bt.getSize());


        // Aanmaken nieuwe Graaf. Bron: Powerpoint SD DT Les 4 figuur 14.16 ( Of Boek Weis ook figuur 14.16)
        Graph graaf = new Graph();
        graaf.addEdge("V0", "V1", 1);
        graaf.addEdge("V0", "V3", 1);
        graaf.addEdge("V1", "V3", 1);
        graaf.addEdge("V1", "V4", 1);
        graaf.addEdge("V2", "V0", 1);
        graaf.addEdge("V2", "V5", 1);
        graaf.addEdge("V3", "V2", 1);
        graaf.addEdge("V3", "V4", 1);
        graaf.addEdge("V3", "V5", 1);
        graaf.addEdge("V3", "V6", 1);
        graaf.addEdge("V4", "V6", 1);
        graaf.addEdge("V6", "V5", 1);

        // graaf.printPath("V5");
        graaf.getVertexName();

        try {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(System.in));

            System.out.print("Enter start node:");
            String startName = reader.readLine();
            System.out.print("Enter destination node:");
            String destName = reader.readLine();
            System.out.print("Enter algorithm (u, d): ");
            String alg = reader.readLine();

            if (alg.equals("u"))
                graaf.unweighted(startName.toUpperCase());
            else if (alg.equals("d"))
                graaf.dijkstra(startName.toUpperCase());
            graaf.printPath(destName.toUpperCase());

        } catch (NoSuchElementException e) {
            System.err.println(e);
        } catch (GraphException e) {
            System.err.println(e);
        } catch (IOException e) {
            System.err.println(e);
        }

    }

}

